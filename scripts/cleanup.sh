#!/usr/bin/env bash

# Cleanup script for the terraform-oidc-azure-gitlab repo

# vars - update these with your own values
APP_REG_NAME='gitlab.com_oidc'
PREFIX='arshzgl'

# login
echo "Logging into Azure..."
az login

# remove role assignment
echo "Removing role assignment..."
APP_CLIENT_ID=$(az ad app list --display-name "$APP_REG_NAME" --query [].appId --output tsv)
SUBSCRIPTION_ID=$(az account show --query id --output tsv)
az role assignment delete --role "Contributor" --assignee "$APP_CLIENT_ID" --scope "/subscriptions/$SUBSCRIPTION_ID"

# remove app reg
echo "Deleting app [$APP_REG_NAME] with App Client Id: [$APP_CLIENT_ID]..."
az ad app delete --id "$APP_CLIENT_ID"

# list then remove resource groups (prompts before deletion)
echo "Deleting these resource groups starting with [$PREFIX] (prompts before deletion)..."
QUERY="[?starts_with(name,'$PREFIX')].name"
az group list --query "$QUERY" --output table
for resource_group in $(az group list --query "$QUERY" --output tsv); do
    echo "Deleting Resource Group: ${resource_group}..."
    az group delete --name "${resource_group}"
done
