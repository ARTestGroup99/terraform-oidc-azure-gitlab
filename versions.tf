terraform {
  # v1.3.4 added AzureRM Backend support for generic OIDC authentication
  # https://github.com/hashicorp/terraform/releases/tag/v1.3.4
  required_version = ">= 1.3.4"

  # https://developer.hashicorp.com/terraform/language/state/remote
  # https://developer.hashicorp.com/terraform/language/settings/backends/azurerm#example-configuration
  backend "azurerm" {
    key = "terraform.tfstate"
  }

  required_providers {
    # https://github.com/terraform-providers/terraform-provider-azurerm/releases
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~> 3.92.0"
    }
  }
}

provider "azurerm" {
  # https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/guides/features-block
  features {}
}
